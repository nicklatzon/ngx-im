/*
 * Public API Surface of ngx-im
 */

export * from './lib/ngx-im.service';
export * from './lib/ngx-im.component';
export * from './lib/ngx-im.module';
