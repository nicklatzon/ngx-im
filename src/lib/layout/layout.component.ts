import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AddChannelComponent } from '../components/modals/add-channel/add-channel.component';
import { IChannel } from '../interfaces/IChannel';
import { DataService } from '../services/data.service';
import { MAX_HEIGHT_IM, MIN_HEIGHT_IM } from './layout.config';
@Component({
  selector: 'im-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LayoutComponent implements OnInit, OnDestroy {
  @ViewChild(AddChannelComponent) addChannelModal: AddChannelComponent;
  channels: IChannel[] = [];
  loading: boolean;
  private _unsubscribeAll: Subject<any>;
  
  constructor(private context: AngularDataContext, public dataService: DataService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.dataService.
    _channels$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => this.channels = val)
    this.fetchChannels();
  };

  fetchChannels() {
    this.loading = true;
    this.context
        .model("MessagingChannels")
        .select("id", "alternateName", "dateCreated", "headline", "thumbnailUrl", "isDirect")
        .expand('keywords')
        .orderByDescending("dateCreated")
        .getItems()
        .then(results => this.dataService.channels = results)
        .catch(error => this.dataService.error = error.message)
        .finally(() => this.loading = false);
  }

  openAddChannel(type: 'channel' | 'direct') {
    this.addChannelModal.openAddChannelModal(type)
  }

  newChannel(channel: IChannel) {
    this.dataService.channels = [
      ...this.channels, 
      { ...channel }
    ]
  }

  styleObject = (): Object => ({minHeight: `${MIN_HEIGHT_IM}px`, maxHeight: `${MAX_HEIGHT_IM}px`})

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
