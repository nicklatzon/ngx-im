export const MIN_HEIGHT_IM = 400; // px
export const MAX_HEIGHT_IM = 700; // px
export const MAX_WIDTH_MESSAGES = 40; // percent
export const MAX_HEIGHT_REPLIES = 60; // viewport percentage (vh)