import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { IChannel, IChannelPost } from '../../../interfaces/IChannel';
import _ from 'lodash';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'im-edit-channel',
  templateUrl: './edit-channel.component.html',
  styleUrls: ['./edit-channel.component.scss']
})
export class EditChannelComponent implements OnInit {
  @ViewChild('editChannelModal') public editChannelModal: ModalDirective;
  @Output() editNewChannel = new EventEmitter<{oldChannel: IChannel; newChannel: IChannel}>();

  form: FormGroup;
  submitted = false;
  keywords: string[] = [];
  loading: boolean = false;
  type: 'channel' | 'direct' | null;
  channel: IChannel = null;

  constructor(private formBuilder: FormBuilder, public dataService: DataService, private context:AngularDataContext) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group(
      {
        headline: ['', [Validators.required]],
        alternateName: ['', [Validators.required]],
        thumbnailUrl: ['', [this.urlValidator()]],
        keyword: [''],
      },
    );
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }
    const { alternateName, headline, thumbnailUrl, email} = this.form.value;

    const data = {
      ...this.channel,
      alternateName,
      headline,
      thumbnailUrl,
      keywords: this.keywords,
      isDirect: this.type === 'direct',
    }
    this.saveApiData(data)
  }


  onReset(): void {
    this.editChannelModal.hide();
    this.submitted = false;
    this.form.get('headline').setValue('')
    this.form.get('alternateName').setValue('')
    this.form.get('thumbnailUrl').setValue('')
    this.form.get('keyword').setValue('')
    this.type = null;
    this.channel = null;
    this.keywords = [];
  }

  openEditChannelModal(type: 'channel' | 'direct', ch: IChannel) {
    const channel =  _.cloneDeep(ch);
    const {headline, alternateName, thumbnailUrl, keywords, members } = channel
    this.form.get('headline').setValue(headline)
    this.form.get('alternateName').setValue(alternateName)
    this.form.get('thumbnailUrl').setValue(thumbnailUrl)
    this.keywords = [...keywords];
    this.type = type;
    this.channel = channel;
    this.editChannelModal.show();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  appendKeyword() {
    const keyword = this.form.get('keyword').value;
    const alreadyIncluded = this.keywords.find(x => x === keyword.trim())
    if(!alreadyIncluded && keyword.trim()) {
      this.keywords.push(keyword.trim());
      this.form.get('keyword').setValue('')
    }
  }

  removeKeyword(keyword: string) {
    this.keywords = this.keywords.filter(x => x !== keyword)
  }

  urlValidator(): ValidatorFn {
    return (control:AbstractControl) : ValidationErrors | null => {
      let validUrl = true;
      const value = control.value;
      try {
        if(value) new URL(value)
      } catch {
        validUrl = false;
      }
  
      return validUrl ? null : { invalidUrl: true };
    }
  }

  saveApiData(data: IChannelPost) {
    this.loading = true;
    this.context
          .model("MessagingChannels")
          .save(data)
          .then(results => {
            const channel: IChannel = results;
            this.editNewChannel.emit({oldChannel: this.channel, newChannel: channel});
            this.onReset();
          })
          .catch(error => this.dataService.error = error.message)
          .finally(() => this.loading = false)
  }
}

