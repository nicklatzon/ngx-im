import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { IMember } from '../../../interfaces/IMember';
import { INewUser } from '../../../interfaces/INewUser';

@Component({
  selector: 'im-add-edit-member-channel',
  templateUrl: './add-edit-member-channel.component.html',
  styleUrls: ['./add-edit-member-channel.component.scss'],
})
export class AddEditMemberChannelComponent implements OnInit {
  @Input() members: IMember[] = [];
  @Input() newMembers: IMember[];
  @ViewChild('addMemberChannelModal') public addEditMemberChannelModal: ModalDirective;
  @Output() newUser = new EventEmitter<INewUser>();
  @Output() editUser = new EventEmitter<{oldUser: INewUser; newUser: INewUser}>();
  currentUser: IMember = null;
  form: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group(
      {
        email: ['', [Validators.required, Validators.email, this.createUniqueEmail()]],
      },
    );
  }

  createUniqueEmail(): ValidatorFn {
    return (control:AbstractControl) : ValidationErrors | null => {
        const value = control.value;

        if (!value) {
            return null;
        }

        const userFounded = [...this.members, ...this.newMembers].find(x => x.name.trim() === value.trim());
        return userFounded ? {notUniqueEmail: true} : null
    }
}

  openAddMembersModal() {
    this.addEditMemberChannelModal.show()
  }

  openEditMembersModal(editUser: IMember) {
    const { name } = editUser;
    this.currentUser = editUser;
    this.form.get('email').setValue(name);
    this.addEditMemberChannelModal.show()
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    const { email } = this.form.value
    const user = {
      name: email,
    }
    this.currentUser ? this.editUser.emit({ oldUser: this.currentUser, newUser: user}) : this.newUser.emit(user)
    this.onReset()
  }

  onReset(): void {
    this.submitted = false;
    this.currentUser = null;
    this.form.get('email').setValue('')
    this.addEditMemberChannelModal.hide();
  }

}
