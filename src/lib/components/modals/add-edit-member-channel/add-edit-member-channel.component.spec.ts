import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditMemberChannelComponent } from './add-edit-member-channel.component';

describe('AddMemberChannelComponent', () => {
  let component: AddEditMemberChannelComponent;
  let fixture: ComponentFixture<AddEditMemberChannelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditMemberChannelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditMemberChannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
