import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentThreadComponent } from './content-thread.component';

describe('ContentThreadComponent', () => {
  let component: ContentThreadComponent;
  let fixture: ComponentFixture<ContentThreadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentThreadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentThreadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
