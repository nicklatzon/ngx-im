import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IChannel, IChannelDelete } from '../../../interfaces/IChannel';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'im-delete-channel',
  templateUrl: './delete-channel.component.html',
  styleUrls: ['./delete-channel.component.scss']
})
export class DeleteChannelComponent implements OnInit, OnDestroy {
  @ViewChild('deleteChannel') public deleteChannel: ModalDirective;
  room: IChannel;
  loading: boolean = false;
  private _unsubscribeAll: Subject<any>;
  constructor(private context:AngularDataContext, private dataService: DataService) { 
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.dataService.
    _selectedChannel$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        if(val) {
          this.room = val;
        }
      })
  }

  openDeleteModal() {
    this.deleteChannel.show()
  }

  delete() {
    const { id } = this.room;
    const data = {
      id,
    }
    this.deleteApiData(data)
  }

  deleteApiData(data: IChannelDelete) {
    this.loading = true;
    this.context
          .model("MessagingChannels")
          .remove(data)
          .then(() => {
            this.deleteChannel.hide()
          })
          .catch(error => this.dataService.error = error.message)
          .finally(() => this.loading = false);
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
