import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentMembersTableComponent } from './current-members-table.component';

describe('CurrentMembersTableComponent', () => {
  let component: CurrentMembersTableComponent;
  let fixture: ComponentFixture<CurrentMembersTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurrentMembersTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentMembersTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
