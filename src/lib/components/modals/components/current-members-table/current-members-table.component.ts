import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { IMember } from 'projects/ngx-im/src/lib/interfaces/IMember';

@Component({
  selector: 'im-current-members-table',
  templateUrl: './current-members-table.component.html',
  styleUrls: ['./current-members-table.component.scss']
})
export class CurrentMembersTableComponent implements OnInit, OnChanges {

  @Input() members: IMember[];
  @Input() maxSize: number;
  @Output() removeEmit = new EventEmitter<IMember>();
  currentUser: IMember;
  
  constructor() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser') || sessionStorage.getItem('currentUser'));
   }

  currentPage: number = 1;
  totalItems: number = 0;

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.members && changes.members.currentValue) {
      if(this.members.length > 0 && this.currentPage > 1 && !this.members.slice((this.currentPage - 1) * this.maxSize, this.currentPage * this.maxSize).length) {
        this.currentPage = this.currentPage - 1;
      }
      this.totalItems = this.members.length;
    }
  }

  pageChanged(event:any) {
    this.currentPage = event.page;
  }

 remove(user: IMember) {
  this.removeEmit.emit(user);
 }

}
