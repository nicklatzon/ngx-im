import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { AngularDataContext } from '@themost/angular';
import { IMember } from '../../../interfaces/IMember';
import { IMessage, IMessagePost } from '../../../interfaces/IMessage';
import { DataService } from '../../../services/data.service';

@Component({
  selector: 'im-content-actions',
  templateUrl: './content-actions.component.html',
  styleUrls: ['./content-actions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContentActionsComponent implements OnInit {
  @Input() selectedId: number;
  @Input() message: IMessage;
  @Input() isReplyChat: boolean;
  @Output() updateMessageReply = new EventEmitter();
  form: FormGroup;
  disabled: boolean = false;
  messages: IMessage[] = [];
  user: IMember;
  quillConfig={
     toolbar: {
       container: [
         ['bold', 'italic', 'underline', 'strike'],
         [{ 'size': ['xsmall', 'small', 'medium', 'large', 'xlarge']}],
         ['clean'], ['link']                     
       ],
     },
  }
  constructor(private context: AngularDataContext, private dataService: DataService) {
    this.user = JSON.parse(localStorage.getItem('currentUser') || sessionStorage.getItem('currentUser'))
   }

  ngOnInit(): void {
    this.form = new FormGroup({
      input: new FormControl('')
    });
    this.isReplyChat ? this.dataService._replies$.subscribe(val => this.messages = val) : this.dataService._messages$.subscribe(val => this.messages = val)
  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }

  strip(html: string) {
    return (new DOMParser().parseFromString(html ?? '', 'text/html').body.textContent || "").length;
 }

 postMessage() {
   this.disabled = true;
   const message: IMessagePost = this.message ? 
   {
      body: this.form.get('input').value.trim(),
      isReply: true,
      parentInstantMessage: this.message.id,
      hasReply: false
   } :
   {
      body: this.form.get('input').value.trim(),
      isReply: false,
      hasReply: false,
   }
   this.context
    .model(`MessagingChannels/${this.selectedId}/messages`)
    .save(message)
    .then(results => {
      const message = {
        ...results,
        reactions: [],
        sender: { ...this.user},
      };
      if(this.message) {
        this.dataService.scrollToBottomThread = true;
        this.dataService.replies = [message, ...this.messages];
        if(!this.message.hasReply) this.updateMessageReply.emit();
      }
      else {
        this.dataService.scrollToBottom = true;
        this.dataService.messages = [message, ...this.messages];
      }
      this.form.get('input').setValue('');
    })
    .catch(error => this.dataService.error = error.message)
    .finally(() => this.disabled = false);
 }
}
