import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentMessagesComponent } from './content-messages.component';

describe('ContentMessagesComponent', () => {
  let component: ContentMessagesComponent;
  let fixture: ComponentFixture<ContentMessagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentMessagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
