import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'im-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ErrorComponent implements OnInit, OnDestroy {
  dismissible: boolean = true;
  dismissOnTimeout: number = 5000;
  alertsDismiss: string[] = [];
  private _unsubscribeAll: Subject<any>;
  
  constructor(public dataService: DataService) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this.dataService.
    _error$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(val => {
        if(val) this.alertsDismiss.push(val)
      });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
