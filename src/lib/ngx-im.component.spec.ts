import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxImComponent } from './ngx-im.component';

describe('NgxImComponent', () => {
  let component: NgxImComponent;
  let fixture: ComponentFixture<NgxImComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgxImComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxImComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
