import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
@Component({
  selector: 'lib-ngx-im',
  template: `<router-outlet></router-outlet>`,
  styles: []
})
export class NgxImComponent implements OnInit {

  constructor(private context: AngularDataContext) {
    const { token } = JSON.parse(localStorage.getItem('currentUser') || sessionStorage.getItem('currentUser')) || { token: { access_token: '' }}
    const { access_token } = token;
    this.context.setBearerAuthorization(access_token);
  }

  ngOnInit(): void {}

}
