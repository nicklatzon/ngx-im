import { IMaintainer } from "./IMaintainer";
import { IMember } from "./IMember";
import { INewUser } from "./INewUser";

export interface IChannel {
    alternateName: string;
    dateCreated: Date;
    dateModified: Date;
    expiresAt: string;
    headline: string;
    maintainer: IMaintainer;
    thumbnailUrl: string;
    keywords: string[];
    isDirect: number | boolean;
    id: number;
    members: IMember[];
}

export interface IChannelPost {
    alternateName: string;
    headline: string;
    thumbnailUrl: string;
    keywords: string[];
    members: INewUser[];
    isDirect?: boolean;
}


export interface IChannelDelete {
    id: number;
}