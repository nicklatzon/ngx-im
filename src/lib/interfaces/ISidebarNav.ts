export type ISidebarItemNav = {
    id: number
    headline: string,
    keywords: string[],
    alternateName: string
}

export type ISidebarNav = {
    title: string,
    type: 'channel' | 'direct',
    collapse: boolean,
    children: ISidebarItemNav[]
}