export interface IMember {
    accountType: number;
    additionalType: string;
    createdBy: number;
    dateCreated: Date;
    dateModified: Date
    description: string;
    id: number;
    modifiedBy: number;
    name: string;
    class?: string;
}