export interface IMaintainer {
    additionalType: string;
    description: string;
    name: string;
    dateCreated: string;
    dateModified: string;
}